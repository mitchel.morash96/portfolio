import conwaysPic from './images/conway.PNG'
import networkedObjectsPic from './images/networked.png'
import asteroidsPic from './images/asteroids.png'
import chartsPic from './images/androidCharts.png'
import bookstorePic from './images/bookstore.png'
import capstonePic from './images/capstone.png'
import portfolioPic from './images/portfolio.PNG'
import webscraperPic from './images/webscraper.PNG'
import BillTrackerPic from './images/BillTracker.PNG'

  // import airportPic from './images/portfolio-4.jpg'
  // import recursivePic from './images/portfolio-5.jpg'
  // import fwAlgorithmPic from './images/portfolio-8.jpg'
  // import kanjiPic from './images/portfolio-9.jpg'


export const projects = [
    {
      id: 1,
      title: "Conway's Game of Life",
      subtitle: "Python",
      description:
        "An assignment done in college to simulate Conway's Game of Life.",
      image: conwaysPic,
      link: "",
      type: "App",
      filter: "col-lg-4 col-md-6 portfolio-item filter-app",
    },
    {
      id: 2,
      title: "3D Networked Objects",
      subtitle: "Java",
      description:
        "A networked app where a server spawns objects & saves them in a database. Then displays them for clients to view.",
      image: networkedObjectsPic,
      link: "",
      type: "App",
      filter: "col-lg-4 col-md-6 portfolio-item filter-app",
    },
    {
      id: 3,
      title: "Asteroids Game",
      subtitle: "JavaScript",
      description:
        "An asteroids game done for an assignment during college.",
      image: asteroidsPic,
      link: "https://gitlab.com/mitchel.morash96/asteroids",
      type: "Web",
      filter: "col-lg-4 col-md-6 portfolio-item filter-web",
    },
    // {
    //   id: 4,
    //   title: "Aiport Simulation",
    //   subtitle: "C++",
    //   description:
    //     "",
    //   image: airportPic,
    //   link: "",
    //   type: "App",
    //   filter: "col-lg-4 col-md-6 portfolio-item filter-app",
    // },
    // {
    //   id: 5,
    //   title: "Recursive Calculator",
    //   subtitle: "C++",
    //   description:
    //     "",
    //   image: recursivePic,
    //   link: "",
    //   type: "app",
    //   filter: "col-lg-4 col-md-6 portfolio-item filter-app",
    // },
    {
      id: 6,
      title: "Android Chart App",
      subtitle: "Android",
      description:
        "An android application that shows the use of a charting libary.",
      image: chartsPic,
      link: "",
      type: "App",
      filter: "col-lg-4 col-md-6 portfolio-item filter-app",
    },
    {
      id: 7,
      title: "Bookstore Database",
      subtitle: "SQL",
      description:
        "A database setup & designed to simulate a bookstore. With a few queries for getting more specific data.",
      image: bookstorePic,
      link: "",
      type: "app",
      filter: "col-lg-4 col-md-6 portfolio-item filter-app",
    },
    // {
    //   id: 8,
    //   title: "Floyd-Warshall Algorithm",
    //   subtitle: "C++",
    //   description:
    //     "",
    //   image: fwAlgorithmPic,
    //   link: "",
    //   type: "App",
    //   filter: "col-lg-4 col-md-6 portfolio-item filter-app",
    // },
    // {
    //   id: 9,
    //   title: "Kanji Number Image Recognition",
    //   subtitle: "Python",
    //   description:
    //     "",
    //   image: kanjiPic,
    //   link: "",
    //   type: "App",
    //   filter: "col-lg-4 col-md-6 portfolio-item filter-app",
    // },
    {
      id: 10,
      title: "Capstone",
      subtitle: "C# & Unity",
      description:
        "A client Server game where players connect to the server and play the game using their phones.",
      image: capstonePic,
      link: "",
      type: "App",
      filter: "col-lg-4 col-md-6 portfolio-item filter-app",
    },
    {
      id: 11,
      title: "Portfolio Site",
      subtitle: "React",
      description:
        "A portfolio site done using React.",
      image: portfolioPic,
      link: "https://gitlab.com/mitchel.morash96/portfolio",
      type: "web",
      filter: "col-lg-4 col-md-6 portfolio-item filter-web",
    },
    {
      id: 12,
      title: "Kijiji Webscraper",
      subtitle: "Python",
      description:
        "A program to get all posts of vehicles on Kijiji & export them to an excel file.",
      image: webscraperPic,
      link: "https://gitlab.com/mitchel.morash96/kijiji-webscraper",
      type: "app",
      filter: "col-lg-4 col-md-6 portfolio-item filter-app",
    },
    {
      id: 13,
      title: "Bill Tracker Site",
      subtitle: "MERN Stack",
      description:
        "A MERN stack application that allows for CRUD operations, and then display the info on front-end.",
      image: BillTrackerPic,
      link: "https://gitlab.com/mitchel.morash96/bill-tracker-app",
      type: "web",
      filter: "col-lg-4 col-md-6 portfolio-item filter-web",
    },
];