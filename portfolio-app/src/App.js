import Navbar from './components/Navbar'
import Hero from './components/Hero'
import About from './components/About'
import Projects from './components/Projects'
import Skills from './components/Skills'
import Contact from './components/Contact'
import Resume from './components/Resume'
import Footer from './components/Footer'

import {skills} from './skill-data'
import {projects} from './project-data'

import 'aos/dist/aos.css'
import './main'

function App() {
  return (
    <main>
      <Navbar />
      <Hero />
      <About />
      <Projects projects={projects}/>
      <Skills skills={skills}/>
      <Resume />
      <Contact />
      <Footer />
    </main>
  );
}

export default App;
