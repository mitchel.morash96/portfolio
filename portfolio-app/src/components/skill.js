import React from 'react'

const Skill = ({ skill }) => {
  return (
    <>
    <div>
      <div className='left'>
        <i className={skill.icon + ' skill-icon'}></i>
      </div>
      <div className="progress">
        <span className="skill">{skill.title} 
          <i className="val">{skill.level}</i>
        </span>
        <div className="progress-bar-wrap">
          <div className="progress-bar" role="progressbar" aria-valuenow={skill.val} aria-valuemin="0" aria-valuemax="100"></div>
        </div>
      </div>
    </div>
    </>
  )
}

export default Skill