import React from 'react'

const Hero = () => {
  return (
    <>
    <section id="hero" className="d-flex flex-column justify-content-center">
        <div className="container" data-aos="zoom-in" data-aos-delay="100">
            <h1>Mitchel Morash</h1>
            <p>I'm a <span className="typed" data-typed-items="Designer, Developer, Learner"></span></p>
            <div className="social-links">
                <a href="https://twitter.com/morash96" className="twitter"><i className="bx bxl-twitter"></i></a>
                {/* <a href="#hero" className="facebook"><i className="bx bxl-facebook"></i></a>
                <a href="#hero" className="instagram"><i className="bx bxl-instagram"></i></a> */}
                {/* <a href="#hero" className="google-plus"><i className="bx bxl-skype"></i></a> */}
                <a href="https://www.linkedin.com/in/mitchel-morash-a32a9a181/" className="linkedin"><i className="bx bxl-linkedin"></i></a>
                <a href="https://gitlab.com/mitchel.morash96" className="gitlab"><i className="bx bxl-gitlab"></i></a>
            </div>
        </div>
    </section>
  </>
  )
}

export default Hero