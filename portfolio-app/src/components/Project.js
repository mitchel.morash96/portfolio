import React from 'react'

const Project = ({ project }) => {
  return (
    <>
    <div className={project.filter}>
        <div className="portfolio-wrap">
            <img src={project.image} className="img-fluid" alt="" />
            <div className="portfolio-info">
                <h4>{project.title}</h4>
                <p>{project.subtitle}</p>
                <div className="portfolio-links">
                  <a href={project.image} data-gallery="portfolioGallery" className="portfolio-lightbox" title="App 1"><i className="bx bx-plus"></i></a>
                  <p>{project.description}</p>
                  {project.link !== '' ? (
                    <a href={project.link} className="portfolio-details-lightbox" data-glightbox="type: external" title={project.title}><i className="bx bx-link"></i></a>
                  ) : null}
                  {/* <a href={project.link} className="portfolio-details-lightbox" data-glightbox="type: external" title={project.title}><i className="bx bx-link"></i></a> */}
                </div>
            </div>
        </div>
    </div>
    </>
  )
}

export default Project