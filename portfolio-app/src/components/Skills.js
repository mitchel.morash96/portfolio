import Skill from "./skill"

const Skills = ({ skills }) => {
  return (
    <>
    <section id="skills" className="skills section-bg">
      <div className="container" data-aos="fade-up">

        <div className="section-title">
          <h2>Skills</h2>
          <p>
            This is a list of languages and technologies I have used on projects.
          </p>
        </div>

        <div className="row skills-content">

          <div className="col-lg-6">
            
            {skills.slice(0, skills.length/2).map((skill) => (
              <Skill key={skill.id} skill={skill} />
            ))}

          </div>

          <div className="col-lg-6">

          {skills.slice(skills.length/2).map((skill) => (
              <Skill key={skill.id} skill={skill} />
            ))}

          </div>

        </div>

      </div>
    </section>
    </>
  )
}

export default Skills