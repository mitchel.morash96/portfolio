import pic from '../images/me.jpg';

const About = () => {
  return (
    <>
      <section id="about" className="about">
        <div className="container" data-aos="fade-up">

          <div className="section-title">
            <h2>About</h2>
            <p>
              I am a recent Honors graduate with a diploma in Information Technology - Programming.
              With experience in writing code for desktop, web &amp; mobile applications.  I have also 
              been involved in maintaining, debugging, and troubleshooting systems and software to 
              ensure that everything is running smoothly.
            </p>
          </div>

            <div className="row">
              <div className="col-lg-4">
                <img src={pic} className="img-fluid avatar" alt="" />
              </div>
              <div className="col-lg-8 pt-4 pt-lg-0 content">
                <h3>Developer.</h3>
                <p className="fst-italic">
                  I am a positive, enthusiastic, and competent Developer who, over the years, has built 
                  up a diverse range of skills, qualities and attributes that guarantee I will perform 
                  highly in a Developer role.
                </p>
                <div className="row">
                  <div className="col-lg-6">
                    <ul>
                      <li><i className="bi bi-chevron-right"></i> <strong>Birthday:</strong> <span>12 October 1996</span></li>
                      <li><i className="bi bi-chevron-right"></i> <strong>Phone:</strong> <span>(902) 890-3078</span></li>
                      <li><i className="bi bi-chevron-right"></i> <strong>City:</strong> <span>Upper Stewiacke, NS</span></li>
                    </ul>
                  </div>
                  <div className="col-lg-6">
                    <ul>
                      <li><i className="bi bi-chevron-right"></i> <strong>Age:</strong> <span>25</span></li>
                      <li><i className="bi bi-chevron-right"></i> <strong>Degree:</strong> <span>Honors Dipolma (GPA: 3.7)</span></li>
                      <li><i className="bi bi-chevron-right"></i> <strong>Email:</strong> <span>mitchel.morash96@gmail.com</span></li>
                    </ul>
                  </div>
                </div>
                <p>
                  I am ambitious, driven, organized and reliable. I thrive on challenges and constantly 
                  set goals for myself, so I have something to strive toward. Which helps me learn new 
                  technologies quickly, So I&apos;m always looking for a new and better way of achieving my goals.
                </p>
              </div>
            </div>

          </div>
      </section>
    </>
  );
}

export default About