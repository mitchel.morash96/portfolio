import React from 'react'

const Footer = () => {
  return (
    <>
    <footer id="footer">
      <div className="container">
        <h3>Mitchel Morash</h3>
        <p></p>
        <div className="social-links">
          <a href="https://twitter.com/morash96" className="twitter"><i className="bx bxl-twitter"></i></a>
          {/* <a href="#" className="facebook"><i className="bx bxl-facebook"></i></a>
          <a href="#" className="instagram"><i className="bx bxl-instagram"></i></a>
          <a href="#" className="google-plus"><i className="bx bxl-skype"></i></a> */}
          <a href="https://www.linkedin.com/in/mitchel-morash-a32a9a181/" className="linkedin"><i className="bx bxl-linkedin"></i></a>
          <a href="https://gitlab.com/mitchel.morash96" className="gitlab"><i className="bx bxl-gitlab"></i></a>
        </div>
        <div className="copyright">
          &copy; Copyright <strong><span>MyResume</span></strong>. All Rights Reserved
        </div>
        <div className="credits">
          {/* All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: [license-url]
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/free-html-bootstrap-template-my-resume/  */}
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
      </div>
    </footer>

    {/* <div id="preloader"></div> */}
    <a href="#hero" className="back-to-top d-flex align-items-center justify-content-center"><i className="bi bi-arrow-up-short"></i></a>
    </>
  )
}

export default Footer