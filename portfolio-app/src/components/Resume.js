import React from 'react'

const Resume = () => {
  return (
    <>
    <section id="resume" className="resume">
      <div className="container" data-aos="fade-up">

        <div className="section-title">
          <h2>Resume</h2>
          <p>

          </p>
        </div>

        <div className="row">
          <div className="col-lg-6">
            <h3 className="resume-title">Sumary</h3>
            <div className="resume-item pb-0">
              <h4>Mitchel Morash</h4>
              <p>
                <em>Recent Honors Graduate, with a GPA 3.7, of a highly ranked College, with programming experience, 
                    skills and knowledge acquired from my 2-Year Information Technology Course. Seeking to use proven skills 
                    in the Information Technology Industry.
                </em>
              </p>
              <ul>
                <li>5441 HWY 289, Upper Stewiacke, NS</li>
                <li>(902) 890-3078</li>
                <li>mitchel.morash96@gmail.com</li>
              </ul>
            </div>

            <h3 className="resume-title">Work Experience</h3>
            <div className="resume-item">
              <h5>August, 2017 - March, 2018</h5>
              <p><em> A&amp;W Resturant, Halifax International Airport, Enfield, NS </em></p>
              <ul>
                <li>Good Communication Skills</li>
                <li>Provided Great Customer Service when dealing with the Public</li>
                <li>Experience working on a Team</li>
                <li>Ability to Thrive under pressure</li>
                <li>Handled Cash (Bondable)</li>
                <li>Demonstrated the ability to work in a Fast-Paced Environment</li>
                <li>Reliable and Punctual</li>
              </ul>
            </div>

          </div>
          <div className="col-lg-6">

            <h3 className="resume-title">Education</h3>
            <div className="resume-item">
              <h4>IT Programming</h4>
              <h5>September, 2018 - June, 2020</h5>
              <p><em> NSCC Centre Of Geographic Scienes, Lawrenecetown, NS </em></p>
              <ul>
                <li>Created a <b>Python</b> application for Conway&apos;s Game of Life</li>
                <li>Developed a <b>Java</b> program that created &amp; displayed networked 3D Objects</li>
                <li>Created a <b>JavaScript</b> application for an Asteroids Game</li>
                <li>Built a <b>C++</b> application for an Airport Simulator program</li>
                <li>Created a <b>C++</b> program for a Recursive Calculator program</li>
                <li>Developed an <b>Android</b> application for a Mobile Calculator</li>
                <li>Developed a <b>SQL</b> Bookstore Database with Queries</li>
                <li>Created Array Functions using <b></b>C</li>
                <li>Developed a Mobile Client-Server Game for my Capstone using <b>Unity</b> and <b>C#</b></li>
                <li>Created a <b>C++</b> application for the Floyd-Warshall Algorithm</li>
                <li>Built a <b>Python</b> program for Kanji Number Image Recognition</li>
              </ul>
            </div>
            
            <h3 className="resume-title">WorkTerm Experience</h3>
            <div className="resume-item">
              <h5>June 1, 2019 - June 30, 2019</h5>
              <p><em> NSCC Centre Of Geographic Scienes, Lawrenecetown, NS - (Reasearch Department) </em></p>
              <ul>
                <li>Worked in a group of two</li>
                <li>Created a data spike using a <b>Raspberry Pi 3B &amp; a DHT11</b> Temperature/Humidity Sensor</li>
                <li>With a goal of placing the data spike outside and sending data to a server, which would display the data on a website</li>
                <li>Developed a website to display data using <b>Html, JavaScript, CSS, Bootstrap &amp; jQuery</b></li>
                <li>Displayed data using <b>Chartist.js</b> graphs</li>
                <li>Preformed daily and weekly write-ups and progress reports</li>
              </ul>
            </div>

          </div>
        </div>

      </div>
    </section>
    </>
  )
}

export default Resume