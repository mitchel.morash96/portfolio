import Project from "./Project"

const Projects = ({projects}) => {
  return (
    <>
      <section id="portfolio" className="portfolio section-bg">
      <div className="container" data-aos="fade-up">

        <div className="section-title">
          <h2>Portfolio</h2>
          <p>
            I have added a selection of projects, I have worked on.
          </p>
        </div>

        <div className="row">
          <div className="col-lg-12 d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
            <ul id="portfolio-flters">
              <li data-filter="*" className="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-web">Web</li>
            </ul>
          </div>
        </div>

        <div className="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

          {projects.map((project) => (
            <Project key={project.id} project={project} />
          ))}

        </div>

      </div>
    </section>
    </>
  )
}

export default Projects